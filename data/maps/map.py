class basemap:
    def __init__(self):
        ''' Init the map, possibly fill it in with grass or somesuch. '''
        self.flags = 0
        self.name = "Unnamed map"
        self.xsize = 0
        self.ysize = 0
    
    def check(self, x, y):
        ''' Check for any special map events -- teleportation, map switching, dialouge, etc. '''
        pass
    
    def dump(self):
        return self.data
    
    def test(self):
        ''' Used to test if the python files are correct (the C embedding simply does not work if there is an error) '''
        return 1
