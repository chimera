#include "chimera.hpp"
extern "C" {
#include "Python.h"
}

int main(int argc, char *argv[]) {
    engine_t engine;
    engine.set_video_mode(800, 600, 16, 0);
    engine.set_fps(30);
    
    Py_Initialize();
    
    PySys_SetPath("data/maps");
    
    PyObject *mapfilename = PyString_FromString("testmap");
    PyObject *mapmodule = PyImport_Import(mapfilename);
    
    PyObject *mapdict = PyModule_GetDict(mapmodule);
    PyObject *mapclass = PyDict_GetItemString(mapdict, "testmap");
    PyObject *mapins = PyObject_CallObject(mapclass, NULL);
    PyObject *maptest = PyObject_CallMethod(mapins, "test", NULL);
    if(PyInt_AsLong(maptest) == 1) {
        cout<<"Python map file valid"<<endl;
    }
    else {
        cout<<"Python map file invalid!"<<endl;
    }
    
    PyObject *mapdump = PyObject_CallMethod(mapins, "dump", NULL);
    
    PyString_AsString(PyList_GetItem(PyList_GetItem(mapdump, 5), 4));
    cout<<PyString_AsString(PyList_GetItem(PyList_GetItem(mapdump, 5), 4))<<endl;
    
    while(!engine.check_key(SDLK_ESCAPE)) {
        engine.check_events();
        engine.framedelay();
    }
    
    Py_DECREF(mapfilename);
    Py_DECREF(mapmodule);
    
    return 0;
}
