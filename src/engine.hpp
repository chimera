#ifndef ENGINE_HPP
#define ENGINE_HPP

#include <iostream>
#include <fstream>
#include <string>
#include <cstdlib>

#include "SDL.h"
#include "SDL_framerate.h"

#include "map.hpp"

class engine_t {
public:
    engine_t(void);
    ~engine_t(void);
    void set_fps(int fpsrate);
    void set_video_mode(int xsize, int ysize, int bpp, bool fullscreen);
    
    int check_key(int whichkey);
    void check_events(void);
    
    void framedelay() {SDL_framerateDelay(&fpsm);}
    void error_log(string message);
    void error_warning(string message);
    void error_fatal(string message);
private:
    SDL_Surface *screen;
    Uint8 keys[SDLK_LAST];
    FPSmanager fpsm;
};

#endif
