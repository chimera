#define PRINT_LOGS
#include "engine.hpp"
#include <string>
#include <bits/basic_string.h>

engine_t::engine_t(void) {
    SDL_Init(SDL_INIT_VIDEO | SDL_INIT_TIMER);
    SDL_initFramerate(&fpsm);
    for(int x = 0; x < SDLK_LAST; x ++) {
        keys[x] = 0;
    }
}

engine_t::~engine_t(void) {
    SDL_Quit();
}

void engine_t::set_fps(int fpsrate) {
    SDL_setFramerate(&fpsm, fpsrate);
}

void engine_t::set_video_mode(int xsize, int ysize, int bpp, bool fullscreen) {
    screen = SDL_SetVideoMode(xsize, ysize, bpp, SDL_HWSURFACE | (fullscreen?SDL_FULLSCREEN:0));
    if(screen == NULL) {
        error_warning("Couldn't set requested screen mode using hardware surface");
        screen = SDL_SetVideoMode(xsize, ysize, bpp, SDL_SWSURFACE | (fullscreen?SDL_FULLSCREEN:0));
        if(screen == NULL) {
            error_warning("Couldn't set requested screen mode using software surface");
            screen = SDL_SetVideoMode(xsize, ysize, bpp, SDL_HWSURFACE);
            error_warning("Couldn't set requested screen mode using hardware surface and non-fullscreen");
            if(screen == NULL) {
                error_warning("Couldn't set requested screen mode using software surface and non-fullscreen");
                screen = SDL_SetVideoMode(xsize, ysize, bpp, SDL_SWSURFACE);
                if(screen == NULL) {
                    error_warning("Couldn't set requested screen mode, using default");
                    xsize = 640;
                    ysize = 480;
                    bpp = 0;
                    screen = SDL_SetVideoMode(xsize, ysize, bpp, SDL_SWSURFACE);
                }
            }
        }
    }
}

void engine_t::check_events(void) {
    SDL_Event event;
    while(SDL_PollEvent(&event)) {
        switch(event.type) {
            case SDL_KEYDOWN: {
                keys[event.key.keysym.sym] = 1;
                break;
            }
            case SDL_KEYUP: {
                keys[event.key.keysym.sym] = 0;
                break;
            }
            case SDL_QUIT: {
                break;
            }
            default: break;
        }
    }
}

int engine_t::check_key(int whichkey) {
    return keys[whichkey];
}

void engine_t::error_log(string message) {
    ofstream logfile("chimera.log", ios_base::app);
    cerr<<message<<endl;
    logfile<<message<<endl;
    logfile.close();
}

void engine_t::error_warning(string message) {
    ofstream logfile("chimera.log", ios_base::app);
    cerr<<message<<endl;
    logfile<<message<<endl;
    logfile.close();
}

void engine_t::error_fatal(string message) {
    ofstream logfile("chimera.log", ios_base::app);
    cerr<<message<<endl;
    logfile<<message<<endl;
    logfile.close();
    exit(1);
}
