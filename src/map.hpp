#ifndef MAP_HPP
#define MAP_HPP

#include <iostream>
#include <fstream>
#include <string>
#include <cstdlib>
#include <memory>
#include <vector>

using std::cout;
using std::endl;
using std::cerr;
using std::string;
using std::ifstream;
using std::ofstream;
using std::ios_base;
using std::vector;

class item_t {
public:
    item_t() : name("Unnamed"), pickable(false), blocking(false) {}
    string get_name() {return name;}
private:
    int connection;
    string name;
    bool pickable;
    bool blocking;
    string image;
};

class map_t {
    int unused;
};

#endif
